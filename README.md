# Visma_FrameworkLockPatch

This module is a patch that solves the issues from [PR #22829](https://github.com/magento/magento2/pull/22829) under Magento 2.3.2. 

## Credits
Thanks to [Ivan Chepurnyi](https://github.com/IvanChepurnyi) who wrote these patches.

## License
MIT
